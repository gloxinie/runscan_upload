### Poetry

The install command reads the pyproject.toml file from the current project, resolves the dependencies, and installs them:

```
poetry install
```

In order to get the latest versions of the dependencies and to update the `poetry.lock` file, you should use the `update` command:

```bash
poetry update
```

The `add` command adds required packages to your `pyproject.toml` and installs them:

```
poetry add [name_of_package]
```



### Lokal

* Lokaler Pfad: `G:\ViconData\BeMoveD_patients`

* In diesem gibt es Unterordner, z.B. `P1234`. Die Zahl kann 3- bis 5-stellig sein.
* In diesem Unterordner gibt es dann
  - eine `.enf`-Datei (diese wird bei der ersten Session angelegt)
  - einen Unterordner `New Session`
  - ggf. weitere Unterordner, die nummeriert sind (`New Session1`, `New Session2`). Diese werden an unterschiedlichen Tagen angelegt. (Es gibt in den `New Session`-Ordnern weitere `.enf`-Dateien, die für das zu entwickelnde Programm aber keine Rolle spielen)



### Server

* Zielpfad: `S:\C09\JWI\MSB\DLST\Patienten\Daten\DLST_P1499\220816`
* Das Datum soll aus einer ``.avi`-Datei ausgelesen werden. Bei jeder Session werden mind. zwei `.avi`-Dateien erstellt, die bspw. den Namen `gang30kmh.2136978.20221128154246.avi`. Hiervon ist `20221128` das Datum.
* In den Zielpfad sollen die `.enf`-Datei (diese kann ggf. auch in einen der Unterordner) und drei Unterordner: `Dokumente`, `GRAIL`, `Vicon`. Die `New Session`-Ordner lösen sich auf.
* In den `GRAIL`-Unterordner soll rein: alle `.avi`-Dateien + die `.mox`-Datei
* In den `Vicon`-Unterordner soll rein: die `.enf`-Datei + alle anderen Dateien mit dem New Session-Unterordner
* Es sollen keine Graphen erstellt werden



### Zum Schluss

* Charité-Pfade aktivieren
