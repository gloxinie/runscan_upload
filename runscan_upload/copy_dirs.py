# Copyright (c) 2022 Cascade Informationssysteme GmbH
# Licensed under the MIT license. See LICENSE.md file in the project root for details.

import logging
import re
import shutil
from pathlib import Path
from typing import List

import PySimpleGUI as sg

from runscan_upload.config import DEST_PATH, OTHER_DIRS, SRC_PATH


def extract_pat_id(path: str) -> str:
    """
    Return pat_id from given local path.
    The path can point to any directory of a local patient's path.
    Return empty string if path does not contain a pat_id or if pat_id is out of range.
    Throws exception if the path is not a string.
    param path: string, path of patient
    return: string, pat_id (patient id)
    """

    assert path != '', 'path is empty'

    pat_id_in_list = re.findall(r'P(\d{3,5}$)', path)  # regex pattern to find 3, 4 or 5 digits after 'P'
    if not pat_id_in_list:
        return ''
    return pat_id_in_list[0]


def local_pat_path(pat_id: str) -> Path:
    """
    return the absolute path to the local patients' directory
    param pat_id: string, patient id
    return: Path, absolute path to local patient dir
    """

    result = SRC_PATH / f'P{pat_id}'
    assert extract_pat_id(str(result)) == pat_id, 'assert in local_pat_path(pat_id) failed'
    return result


def remote_pat_path(pat_id: str) -> str:
    """
    return the absolute path to the remote patients' directory
    :param pat_id: string, patient's id
    :return: string, absolute path to the remote patient's directory
    """

    result = f'{DEST_PATH}{pat_id}'
    assert extract_pat_id(result) == pat_id, 'assert in remote_pat_path(pat_id) failed'
    return result


def fetch_local_dirs(pat_id: str) -> List[Path]:
    """
    Return list of strings with directories in local patient dir on RunScan computer.
    param pat_id: string, patient's id
    return: list of strings containing directory-names always beginning with "New Session", e.g. ['New Session',
    'New Session1', New Session2']
    """

    path = Path(local_pat_path(pat_id))

    if path.exists():
        list_local_dirs = [i for i in path.iterdir() if i.is_dir()]
        return list_local_dirs


def avi_files_list(pat_id: str) -> bool:
    """
    function checks if there are .avi-files in local directory of given patient.
    param pat_id: string, patient's id
    return files: list of Paths of .avi-files
    """

    assert pat_id != '', 'pat_id is empty'

    local_dirs = fetch_local_dirs(pat_id)  # get list of New Session dirs

    if local_dirs is not None:
        for src_path in local_dirs:  # iterate through New Session dirs
            avi_list_for_each_subdir = list(src_path.glob('*.avi'))  # get .avi-file for each 'New Session'

            if avi_list_for_each_subdir:
                return True
            else:
                return False


def date_from_first_avi(path: Path, pat_id: str) -> str:
    """
    Return date from given path, which in this case is the first .avi-file found.
    Error message if path is empty string or no valid date structure can be found.
    param path: pathlib:WindowsPath, file path
    param pat_id: string, patient's id
    return: string, 6-digit date
    """

    assert path != '', 'path is empty'

    if avi_files_list(pat_id):
        first_avi_file = list(path.glob('*.avi'))[0]  # get first .avi-file in 'New Session' dir,
        # e.g. C:\Git\runscan_upload\P1589\New Session\New Session101.2136978.20221128125254.avi
        stem_date = first_avi_file.stem[-12:-6]  # get date from stem of .avi-file, e.g. 20221128
        return stem_date
    else:
        sg.popup(
            f'\n.Es wurden keine .avi-Dateien im Pfad\n{path} gefunden.\n'
            f'Bitte "Vicon Transfer" drücken.\n'
            f'Der Kopiervorgang wird unterbrochen.\n'
            f'\n\n', title='Hinweis', custom_text='Okay')


def create_subdirs(path: Path, date: str):
    """
    Create subdirectories on server.
    Error message if path is empty string or no valid date structure can be found.
    Popup in case the server directory exists already (FileExistsError).
    param path: pathlib:WindowsPath, file path
    param date: string, 6-digit date
    """

    assert path != '', 'path is empty'

    for other_dir in OTHER_DIRS:
        try:
            subdir = Path(path / date / other_dir)
            subdir.mkdir(parents=True, exist_ok=True)
        except FileExistsError as e:
            logging.warning(f'83. directory exists already, {e}')
            sg.popup(
                    f'\nDer Ordner dieses Pfads\n{path / other_dir / date}\n'
                    f'existiert bereits.'
                    f'Der Kopiervorgang wird unterbrochen.\n'
                    f'\n\n', title='Hinweis', custom_text='Okay')
        except FileNotFoundError as e:
            logging.warning(f'84. missing parent(s) of path, {e}')


def copy_files(path_src: Path, path_dest: Path, date: str):
    """
    Copy .enf-file in root of patient directory on server.
    Copy .avi- and .mox-files into GRAIL-subdirectory on server.
    Copy all other files in Vicon-subdirectory on server.
    Error message if paths or date are empty strings.
    Popup in case the server directory exists already (FileExistsError).
    param path_src: pathlib:WindowsPath, file path
    param path_dest: pathlib:WindowsPath, file path
    param date: string, 6-digit date
    """

    assert path_src != '', 'path_src is empty'
    assert path_dest != '', 'path_dest is empty'
    assert date != '', 'date is empty'

    enf_file = [i for i in path_src.parent.glob('*.enf')]  # get .enf-file in root-dir of patient
    shutil.copy2(enf_file[0], Path(path_dest))  # copy .enf-file in root-dir of patient on server

    files_list = [file for file in path_src.iterdir()]  # put all files in New Session dirs in list
    for file in files_list:
        if file.suffix == '.avi' or file.suffix == '.mox':
            shutil.copy2(file, Path(path_dest / date / 'GRAIL'))
        else:
            shutil.copy2(file, Path(path_dest / date / 'Vicon'))


def copy_local_to_remote(pat_id: str):
    """
    Copy all local data of patient to server.
    Error message if paths or date are empty strings.
    param pat_id: string, patient's id
    """

    assert pat_id != '', 'pat_id is empty'

    local_dirs = fetch_local_dirs(pat_id)  # get list of New Session dirs
    dest_path = Path(remote_pat_path(pat_id))

    if local_dirs is not None:
        for src_path in local_dirs:  # iterate through New Session dirs
            date = date_from_first_avi(src_path, pat_id)  # get date for each 'New Session'
            create_subdirs(dest_path, date)  # create subdirs 'Dokumente', 'GRAIL', 'Vicon'
            copy_files(src_path, dest_path, date)  # copy files from local to remote