# Copyright (c) 2022 Cascade Informationssysteme GmbH
# Licensed under the MIT license. See LICENSE.md file in the project root for details.

from pathlib import Path

# LOCAL:
# SRC_PATH = Path(r'C:/Git/runscan_upload')
# DEST_PATH = Path(r'C:/Git/runscan_upload/TEST/Daten/DLST_P')

# CHARITÉ:
SRC_PATH = Path('G:/ViconData/BeMoveD_patients')
DEST_PATH = Path('S:/C09/JWI/MSB/DLST/Patienten/Daten/DLST_P')

# SUBDIRECTORIES ON SERVER
OTHER_DIRS = ['Dokumente', 'GRAIL', 'Vicon']
