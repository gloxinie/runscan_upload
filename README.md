# About RunScan_upload

[toc]



## General info

**RunScan_upload** copies the local data from RunScan to the BeMoveD-server of Charité Berlin.

A specific folder structure is created with date and three subdirectories.

The date is retrieved from the first .avi-file found in each 'New Session' directory.

If the directory exists already, copying is not possible.


## Technologies

The code is written in Python 3.11

Additional libraries installed and used are PySimpleGui (4.60.4) and isort (5.10.1).


## Create .exe-file for Windows OS

`cd` into dir `runscan_upload`. Use PyInstaller to create one single file:

```
pyinstaller -F -w --icon=icon31.ico --name 'RunScan_upload' gui.py
```

With target:

```
TARGET=charite pyinstaller -F -w --icon=../icon31.ico --name 'RunScan_upload' gui.py
```

If you want to create several files:

```
pyinstaller -w --icon=../icon31.ico --name 'RunScan_upload' gui.py
```

If you are on a Windows computer you get a file called `RunScan_upload.exe`.



## License
[![License](http://img.shields.io/:license-mit-blue.svg?style=flat-square)](http://badges.mit-license.org)